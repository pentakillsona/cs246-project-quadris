#ifndef ZBLOCK_H
#define ZBLOCK_H

#include "Block.h"

class Board;

class ZBlock: public Block {
public:
	ZBlock(Board*, int);
	~ZBlock();
};


#endif