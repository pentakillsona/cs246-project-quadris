#include "SBlock.h"
#include "Board.h"

using namespace std;

SBlock::SBlock(Board *b, int lvl) {
    type = 'S';
    board = b;
    level = lvl;

    for (int c = 0; c < 2; ++c) {
        if (board->dropped(4, c)) {
            cells.clear();
            return;
        } else {
            cells.emplace_back(board->getCell(4, c));
        }
    }

    for (int c = 1; c < 3; ++c) {
        if (board->dropped(3, c)) {
            cells.clear();
            return;
        } else {
            cells.emplace_back(board->getCell(3, c));
        }
    }

    for (auto c: cells) c->fill(this->type);
}


SBlock::~SBlock() {}