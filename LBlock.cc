#include "LBlock.h"
#include "Board.h"

using namespace std;

LBlock::LBlock(Board *b, int lvl) {
    type = 'L';
    board = b;
    level = lvl;

    for (int c = 0; c < 3; ++c) {
        if (board->dropped(4, c)) {
            cells.clear();
            return;
        } else {
            cells.emplace_back(board->getCell(4, c));
        }
    }
    
    if (b->dropped(3, 2)) {
        cells.clear();
        return;
    } else {
        cells.emplace_back(board->getCell(3, 2));
    }

    for (auto c: cells) c->fill(this->type);
}


LBlock::~LBlock() {}