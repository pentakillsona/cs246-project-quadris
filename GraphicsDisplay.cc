#include "GraphicsDisplay.h"

GraphicsDisplay::GraphicsDisplay() :
  window{},
  level(0),
  score(0),
  hiscore(0) {
}

void GraphicsDisplay::init() {
  window.fillRectangle(0,0,500,500,Xwindow::White);
  window.fillRectangle(10, 10, 352, 480, Xwindow::Grey);
  drawGameInfo();
}

void GraphicsDisplay::notify(Subject<State> &whoNotified)  {
  State s = whoNotified.getState();
  if (s.type != ' ') {
    if (s.type == 'I') {
      window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Cyan);
    } else if (s.type == 'J') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Blue);
    } else if (s.type == 'L') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Orange);
    } else if (s.type == 'O') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Yellow);
    } else if (s.type == 'S') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Green);
    } else if (s.type == 'T') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Purple);
    } else if (s.type == 'Z') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Magenta);
    } else if (s.type == '*') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Brown);
    } else if (s.type == '?') {
        window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Black);
    }
  } else {
    window.fillRectangle(s.c*32+10, (s.r-3)*32+10, 32, 32, Xwindow::Grey);
  }
}

void GraphicsDisplay::update(int level, int score, int hiscore, std::string next) {
  this->level = level;
  this->score = score;
  this->hiscore = hiscore;
  this->next = next;
  drawGameInfo();
}

void GraphicsDisplay::drawGameInfo() {
  window.fillRectangle(370, 0, 130, 130, Xwindow::White);
  window.drawString(370, 30, "level:     ");
  window.drawString(370, 50, "score:     ");
  window.drawString(370, 70, "hi score:  ");
  window.drawString(440, 30, std::to_string(level));
  window.drawString(440, 50, std::to_string(score));
  window.drawString(440, 70, std::to_string(hiscore));
  window.fillRectangle(380, 90, 90, 90, Xwindow::Grey);
  drawTetramino(next);
}

void GraphicsDisplay::sync() {
  window.sync();
}

void GraphicsDisplay::drawTetramino(std::string next) {
  if (next == "I") {
    window.fillRectangle(395, 127, 60, 15, Xwindow::Cyan);
  } else if (next == "L") {
    window.fillRectangle(398, 135, 54, 18, Xwindow::Orange);
    window.fillRectangle(434, 117, 18, 18, Xwindow::Orange);
  } else if (next == "J") {
    window.fillRectangle(398, 135, 54, 18, Xwindow::Blue);
    window.fillRectangle(398, 117, 18, 18, Xwindow::Blue);
  } else if (next == "O") {
    window.fillRectangle(405, 115, 42, 42, Xwindow::Yellow);
  } else if (next == "T") {
    window.fillRectangle(398, 135, 54, 18, Xwindow::Purple);
    window.fillRectangle(416, 117, 18, 18, Xwindow::Purple);
  } else if (next == "S") {
    window.fillRectangle(398, 135, 36, 18, Xwindow::Green);
    window.fillRectangle(416, 117, 36, 18, Xwindow::Green);
  } else if (next == "Z") {
    window.fillRectangle(398, 117, 36, 18, Xwindow::Magenta);
    window.fillRectangle(416, 135, 36, 18, Xwindow::Magenta);
  }

}
