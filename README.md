[![Build Status](https://travis-ci.com/DannyKong12/cs246-project-quadris.svg?token=F2tHBq1i61cXB7i457Rq&branch=master)](https://travis-ci.com/DannyKong12/cs246-project-quadris)
# cs246-project-quadris
## Project Breakdown
Our design for Quadris will follow the Observer design pattern. We will have classes for each of the main components with the following roles:
* `Observer`
Abstract base class for observers

* `Subject`
Abstract base class for subjects

* `Block`
Abstract base class for blocks. Contains pure virtual methods for moving the blocks (left, right, down, drop, clockwise, counterclockwise). Concrete subclasses (L, S, I, Z, T, O, J) inherit from this class.

* `Board`
The board class contains the grid of cells, the text display or graphics display, and the current and next block. When a game is started, the main function will create a `Board`, and issue commands to it.

* `State`
State will be a struct containing information about the current state of the game, such as level, score, and high score.

* `Cell`
Cell is a class that holds information about each individual cell on the board, such as the level it was created in, the type of block it came from, filled or unfilled.

* `Textdisplay` and `GraphicsDisplay`
keeps track of the character grid to be displayed on the screen or the graphical display of the board.

## Schedule
We will implement the abstract base classes for the observer pattern first, as many components rely on that class. We will then implement block class and concrete block classes, base features of board and cells first, as they are necessary for the minimally functioning game with a very simple main function. We will be following a tenative schedule as follows:

#### July 16th-18th (Due Date 1):
* Header files with documentation
* Observer/Subject classes

#### July 18th-20th:
* Note any changes to header files
* Progress update
* Revisit splitting of work

#### July 20th-22nd (Team meeting):
* Finish implementation including classes:
    * Rest of Board
    * GraphicsDisplay
    * Levels 1-4 (levelUp, levelDown, newBlock)
* Make sure individual classes compile
* Test code individually first
* Testing/debugging
#### July 22nd-25th (Compiled project):
* Extra credit features
* Documentation/report

#### Due date: July 25th:

## Roles
#### Danny
* Observer, Subject
* Documentation
* Board (init, left, right, levelUp, levelDown)
* Cell

#### Frank
* Board (newBlock, down, drop)
* TextDisplay
* GraphicsDisplay

#### Romahlio
* Block
* concrete blocks


## Questions
1. **Q:** How could you design your system (or modify your existing design) to allow for some generated blocks to disappear from the screen if not cleared before 10 more blocks have fallen? Could the generation of such blocks be easily confined to more advanced levels?
**A:**  Since we designed each cell to have its own wrapper class, we can store metadata about the time it was created. The board itself can store the number of blocks generated thus far, and as soon as a block is placed, it will update the cells. That will allow us to know when to make the block disappear after x number of blocks are generated. We also plan to store the level a block is created, so it will be easily confined.
<br>
1. **Q:** How could you design your program to accommodate the possibility of introducing additional levels into the system, with minimum recompilation?
**A:** All the functions that are dependent on level will be implemented with access to the current state of the game including level. The board and functions like `newBlock` will change it's behaviour depending on the level. When we add a level, we would add it to a level enum, and add a switch case to each function affected by level.
<br>
1. **Q:** How could you design your system to accommodate the addition of new command names, or changes to existing command names, with minimal changes to source and minimal recompilation? (We acknowledge, of course, that adding a new command probably means adding a new feature, which can mean adding a non-trivial amount of code.) How difficult would it be to adapt your system to support a command whereby a user could rename existing commands (e.g. something like rename counterclockwise cc)? How might you support a “macro” language, which would allow you to give a name to a sequence of commands?
**A:** We will implement a macro language by having a vector of macro structs, which are composed of a name, and a sequence of methods, e.g. ("complex", [left, left, drop]). We can add a new name for a default function by adding a macro to the vector with a name, and a sequence of a single action, e.g. ("h", [left]), and rename a command by searching through the vector for the command and changing its name if found. The main function will loop through the vector, and call the functions named by the sequence if the input matches a macro name.
