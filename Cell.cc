#include "Cell.h"



Cell::Cell(int r, int c) : r(r), c(c) {
	State s{ r,c,' ',false, 0, 0};
	setState(s);
}

Cell::~Cell()
{
}

bool Cell::filled() {
	if (getState().type == ' ')return false;
	return true;
}

bool Cell::dropped() {
	return getState().dropped;
}

void Cell::empty() {
	State s = getState();
	if(s.type!='?')s.type = ' ';
	s.dropped = false;
	setState(s);
	notifyObservers();
}

void Cell::fill(char ch) {
	State s{ r,c,ch,false };
	setState(s);
	notifyObservers();
}

void Cell::drop(int level, int id){
	State s = getState();
	s.dropped = true;
  s.level = level;
  s.blockID = id;
	setState(s);
}

void Cell::clone(Cell &other) {
  State s = {getState().r, getState().c, 'n', true, 0, 0};
  s.type = other.getState().type;
  s.dropped = other.getState().dropped;
  s.level = other.getState().level;
  s.blockID = other.getState().blockID;
  setState(s);
  notifyObservers();
}

int Cell::getLevel() {
  return getState().level;
}

int Cell::getID() {
  return getState().blockID;
}
