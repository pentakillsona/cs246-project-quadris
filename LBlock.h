#ifndef LBLOCK_H
#define LBLOCK_H

#include "Block.h"

class Board;

class LBlock: public Block {
public:
	LBlock(Board*, int);
	~LBlock();
};


#endif