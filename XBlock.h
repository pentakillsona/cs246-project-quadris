#ifndef XBLOCK_H
#define XBLOCK_H

#include "Block.h"

class XBlock : public Block {
public:
	XBlock(Board*, int);
	~XBlock();
};

#endif