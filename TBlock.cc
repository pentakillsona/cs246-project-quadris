#include "TBlock.h"
#include "Board.h"

using namespace std;

TBlock::TBlock(Board *b, int lvl) {
    type = 'T';
    board = b;
    level = lvl;

    if (board->dropped(4, 1)) {
        cells.clear();
        return;
    } else {
        cells.emplace_back(board->getCell(4, 1));
    }
    
    for (int c = 0; c < 3; ++c) {
        if (b->dropped(3, c)) {
            cells.clear();
            return;
        } else {
            cells.emplace_back(board->getCell(3, c));
        }
    }
    
    for (auto c: cells) c->fill(this->type);
}

TBlock::~TBlock() {}