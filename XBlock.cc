#include "XBlock.h"
#include "Board.h"

using namespace std;

XBlock::XBlock(Board *b, int lvl) {
	type = '*';
	board = b;
	level = lvl;

	if (board->dropped(3, 5)) {
		cells.clear();
	}
	else {
		cells.emplace_back(board->getCell(3, 5));
	}

	for (auto c : cells) c->fill(this->type);
}


XBlock::~XBlock() {}
