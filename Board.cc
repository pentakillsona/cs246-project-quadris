#include "Board.h"
#include <time.h>
#include <math.h>


Board::Board()
{
	commands = { "left", "right", "down", "clockwise", "counterclockwise", "drop",
		"levelup", "leveldown", "norandom", "random", "sequence",
		"I" , "J" , "L" , "O" , "S" , "T" , "Z", "restart", "hint" };
}


Board::~Board()
{
	theBoard.clear();
	refs.clear();
	delete td;
	delete gd;
}

void Board::init(int startLevel, int seed, bool textFlag, std::string file) {
	level = startLevel;
	israndom = true;
	badturnCount = 0;
	blockNum = -1;
	runFile = false;
	score = 0;
	text = textFlag;
  blockList = file;
  readCount = 0;
  for (unsigned int i = 0; i < theBoard.size(); i++) 
    for (unsigned int j = 0; j < theBoard[i].size(); j++) 
      getCell(i, j)->fill(' ');
	theBoard.clear();
  if (td) {
    delete td;
  }
	td = new TextDisplay();
	if (gd) {
    gd->init();
	} else if (!text) {
    gd = new GraphicsDisplay();
    gd->init();
  }
	for (int i = 0; i < numRows + 3; i++) {
		std::vector<Cell> v;
		for (int j = 0; j < numCols; j++) {
			v.emplace_back(Cell(i, j));
		}
		theBoard.emplace_back(v);
	}
	for (int i = 3; i < numRows + 3; i++) {
		for (int j = 0; j < numCols; j++) {
			theBoard[i][j].attach(td);
			if (!text) {
	      theBoard[i][j].attach(gd);
			}
		}
	}
	myfile=std::ifstream(file);
	if (!myfile.is_open()) {
    throw "File " + file + " not found!";
  }
  srand(seed);
  newBlock(blockSelector());
	newBlock(blockSelector());
	Command();
}

std::string Board::readFile() {
	if (myfile.is_open()) {
		std::string word;
		if (readCount == 0) {
			if (!(myfile >> word)) std::cout << "Empty file";
			readCount++;
			return word;
		}
		if (myfile >> word) {
			return word;
		} else {
      myfile.close();
      myfile = std::ifstream(blockList);
      return readFile();
    }
  }
	return "";
}

std::string Board::blockSelector() {
	std::string word;
	if (level == 0)return readFile();
	else if (level == 1) {
		std::string words[12] = { "I", "J", "L", "O",  "T","I", "J", "L", "O",  "T","S", "Z" };
		return words[rand() % 12];
	}
	else if (level == 2) {
		std::string words[7] = { "I", "J", "L", "O",  "T","S", "Z" };
		return words[rand() % 7];
	}
	else if (level >= 3) {
		if (israndom) {
			std::string words[9] = { "I", "J", "L", "O",  "T","S", "Z","S", "Z" };
			return words[rand() % 9];
		}
		else return readFile();
	}
	return "";
}

void Board::newBlock(std::string word) {
	if (blockNum == -1) {
		next = word;
		blockNum++;
		return;
	}
	if (next == "I")block = new IBlock(this, level);
	else if (next == "J")block = new JBlock(this, level);
	else if (next == "L")block = new LBlock(this, level);
	else if (next == "O")block = new OBlock(this, level);
	else if (next == "S")block = new SBlock(this, level);
	else if (next == "T")block = new TBlock(this, level);
	else if (next == "Z")block = new ZBlock(this, level);

	if (block->isempty()) 
		if (next != "") {
      if (gd) {
        gd->update(level, score, hiscore, next);
      }
			std::cout << *this;
      std::cout << "Game Over" << std::endl;
      std::cout << "Play again? y/n" << std::endl;
      std::string command;
      while (std::cin >> command) {
        if (command == "y" || command == "yes") {
          init(0, 0, text, blockList);
        } else if (command == "n" || command == "no") {
          exit(0);
        } else 
          std::cout << "Play again? y/n" << std::endl;
      }
	}
	if(next==""){
    if (gd) {
      gd->update(level, score, hiscore, next);
    }
		std::cout << *this;
		throw "End-of-file (EOF)! The game is terminated.";
	} else lvChange = false;
	next = word;
}

void Board::Command(){
	if (!runFile) {
		std::string command;
    if (gd) {
      gd->update(level, score, hiscore, next);
    }
		std::cout << *this;
		std::cout << "move: ";
		std::cin >> command;
    if (std::cin.fail()) {
      std::cout << "\nThanks for Playing!" << std::endl;
      exit(0);
    }
		std::cout << std::endl;
		interpretCommand(command);
		if(input == "restart" || input == "hint" || input == "norandom" || input == "random"|| input == "sequence")
			runCommand(input);
		else for (int i = 0;i < numTimes;i++)runCommand(input);

	if (level >= 3)
		if (input == "left" || input == "right" || input == "down" || input == "clockwise" || input == "counterclockwise")
			block->down();
		Command();
	}
	else {
		if (commandFile.is_open())
		{
			std::string word;
			while (commandFile >> word) {
				interpretCommand(word);
			if(input == "restart" || input == "hint" || input == "norandom" || input == "random")
				runCommand(input);
			else for (int i = 0;i < numTimes;i++)runCommand(input);

	if (level >= 3)
		if (input == "left" || input == "right" || input == "down" || input == "clockwise" || input == "counterclockwise")
			block->down();
			}
			commandFile.close();
			runFile = false;
			Command();
		}
		else std::cout << "Unable to open file";
	}
}

void Board::interpretCommand(std::string command) {
	unsigned int digits = 0;
	numTimes = 1;

	for (; digits < command.length(); ++digits) {
		if (isdigit(command[digits]) == 0) {
			break;
		}
	}

	if (digits > 0) {
		numTimes = stoi(command.substr(0, digits));
		command = command.substr(digits, command.length());
	}

	int matched = 0;

	for (auto it : commands) {
		if (it.compare(0, command.length(), command) == 0) {
			if (matched == 0) {
				input = it;
				++matched;
			}
			else {
				input = "";
			}
		}
	}
}

void Board::runCommand(std::string command){
	block->hintClear();

	if (command == "left")block->left();
	else if (command == "right")block->right();
	else if (command == "down")block->down();
	else if (command == "clockwise")block->clockwise();
	else if (command == "counterclockwise")block->counterclockwise();
	else if (command == "drop") {
		block->drop(level, blockNum++);
		refs.emplace_back(4);
		delete block;
		if (clear())badturnCount = 0;
		else if (level == 4) {
			badturnCount++;
			if (badturnCount >= 5) {
				badturnCount = 0;
				block = new XBlock(this, 4);
				block->drop(level, blockNum++);
				delete block;
				refs.emplace_back(4);
			}
		}
		newBlock(blockSelector());
		return;
	}
	else if (command == "levelup") {
		if (level < 4) {
			levelup();
			return;
		}
	}
	else if (command == "leveldown") {
		if (level > 0) {
			leveldown();
			return;
		}
	}
	else if (command == "norandom") {
std::string file;
std::cin >> file;
norandom(file);
	}
	else if (command == "random") random();
	else if (command == "sequence") {
		std::string file;
		std::cin >> file;
		sequence(file);
		return;
	}
	else if (command == "I" || command == "J" || command == "L" || command == "O" || command == "S" || command == "T" || command == "Z")
		replaceBlock(command);
	else if (command == "restart") init(0, 0, text, blockList);
	else if (command == "hint") hint();
}

bool Board::clear() {
	int clearCount = 0;
	for (int i = 0;i < numRows + 3;i++) {
		bool filled = true;
		for (int j = 0; j < numCols; j++) {
			if (!theBoard[i][j].filled()) {
				filled = false;
				break;
			}
		}
		if (filled) {
			clearCount++;
			clearLine(i);
		}
	}
	updateScore(clearCount);
	if (clearCount > 0)return true;
	return false;
}

void Board::clearLine(int n) {
	for (int i = 0; i < numCols; i++) {
		if (--refs[theBoard[n][i].getID()] == 0) {
			score += (int)pow((theBoard[n][i].getLevel() + 1), 2);
		}
		theBoard[n][i].empty();
	}
	for (int i = n; i > 0; i--) {
		for (int j = 0; j < numCols; j++) {
			theBoard[i][j].clone(theBoard[i - 1][j]);
		}
	}
	for (int i = 0; i < numCols; i++) {
		theBoard[0][i].empty();
	}
}

void Board::updateScore(int clearCount) {
	if (clearCount > 0) {
		score += (int)pow((level + clearCount), 2);
	}
	if (score > hiscore) {
		hiscore = score;
	}
  if (!text) {
		gd->update(level, score, hiscore, next);
	}
}

void Board::levelup() {
	level++;
	lvChange=true;
	if (!text) {
		gd->update(level, score, hiscore, next);
	}
	if(israndom)myfile.close();
}

void Board::leveldown() {
	level--;
	lvChange=true;
	if (!text) {
		gd->update(level, score, hiscore, next);
	}
}

void Board::norandom(std::string file) {
	if (level >= 3) {
		israndom = false;
		blockFile = file;
		myfile = std::ifstream(file);
		next = blockSelector();
	}
}

void Board::random() {
	if (level >= 3) {
		israndom = true;
	}
	next = blockSelector();
}

void Board::sequence(std::string file) {
	runFile = true;
	commandFile = std::ifstream(file);
}

void Board::replaceBlock(std::string s) {
	block->empty();
	delete block;
	if (s == "I")block = new IBlock(this, level);
	else if (s == "J")block = new JBlock(this, level);
	else if (s == "L")block = new LBlock(this, level);
	else if (s == "O")block = new OBlock(this, level);
	else if (s == "S")block = new SBlock(this, level);
	else if (s == "T")block = new TBlock(this, level);
	else if (s == "Z")block = new ZBlock(this, level);
}

void Board::hint() {
	block->save();

	int max = -400, move, rotate;
	for (int i = 0;i <= 10;i++) {
		for (int j = 0;j <= 3;j++) {
			for (int k = 0;k < j;k++)block->clockwise();
			for (int k = 0;k < i;k++) block->left();
			block->drop(level, blockNum);
			int c = hintScore();
			if (c > max) {
				max = c;
				move = -i;
				rotate = j;
			}
			block->undo();
		}
	}
	for (int i = 1;i <= 10;i++) {
		for (int j = 0;j <= 3;j++) {
			for (int k = 0;k < j;k++)block->clockwise();
			for (int k = 0;k < i;k++) block->right();
			block->drop(level, blockNum);
			int c = hintScore();
			if (c > max) {
				max = c;
				move = i;
				rotate = j;
			}
			block->undo();
		}
	}
	if(move<=0){
		for (int k = 0;k < rotate;k++)block->clockwise();
		for (int k = 0;k < -move;k++) block->left();
		block->drop(level, blockNum);
		block->fill('?');
		block->hintSave();
		block->undo();
	}else {
		for (int k = 0;k < move;k++) block->right();
		for (int k = 0;k < rotate;k++)block->clockwise();
		block->drop(level, blockNum);
		block->fill('?');
		block->hintSave();
		block->undo();
	}
}

int Board::hintScore() {
	int clearCount = 0;
	for (int r = 0;r < numRows + 3;r++) {
		bool filled = true;
		for (int c = 0; c < numCols; c++) {
			if (!theBoard[r][c].filled()) {
				filled = false;
				break;
			}
		}
		if (filled) {
			clearCount++;
		}
	}
	int emptyCount = 0;
	for (int r = 0;r < numRows + 3;r++) {
		bool empty = true;
		for (int c = 0; c < numCols; c++) {
			if (theBoard[r][c].filled()) {
				empty = false;
				break;
			}
		}
		if (empty) {
			emptyCount++;
		}
	}
	return clearCount * 10000 - block->flyCount() * 100 + emptyCount;
}

bool Board::filled(int row, int col) {
	return theBoard[row][col].filled();
}

bool Board::dropped(int row, int col) {
	return theBoard[row][col].dropped();
}

Cell* Board::getCell(int row, int col) {
	return &theBoard[row][col];
}

std::ostream &operator<<(std::ostream &out, const Board &b) {
	std::cout << "Level:\t\t" << b.level << std::endl;
	std::cout << "Score:\t\t" << b.score << std::endl;
	std::cout << "Hi Score:\t" << b.hiscore << std::endl;
	std::cout << *b.td;
  if (b.gd) {
    b.gd->sync();
  }
	std::cout << "Next:\t" << b.next << std::endl;
	return out;
}
