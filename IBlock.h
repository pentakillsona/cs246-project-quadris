#ifndef IBLOCK_H
#define IBLOCK_H

#include "Block.h"

class IBlock: public Block {
public:
	IBlock(Board*, int);
	~IBlock();
};

#endif