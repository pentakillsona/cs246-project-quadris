#include "TextDisplay.h"
#include "subject.h"

TextDisplay::TextDisplay() {
	theDisplay.clear();

	std::vector<char> v;
	v.emplace_back('+');
	for (int j = 1; j <= 11; j++) {
		v.emplace_back('-');
	}
	v.emplace_back('+');
	theDisplay.emplace_back(v);

	for (int i = 0; i < 15; i++) {
		v.clear();
		v.emplace_back('|');
		for (int j = 1; j <= 11; j++) {
			v.emplace_back(' ');
		}
		v.emplace_back('|');
		theDisplay.emplace_back(v);
	}

	v.clear();
	v.emplace_back('+');
	for (int j = 1; j <= 11; j++) {
		v.emplace_back('-');
	}
	v.emplace_back('+');
	theDisplay.emplace_back(v);
}

void TextDisplay::notify(Subject<State> &whoNotified)  {
	if(whoNotified.getState().r>=3)
	theDisplay[whoNotified.getState().r -2][whoNotified.getState().c + 1] = whoNotified.getState().type;
}

std::ostream &operator<<(std::ostream &out, const TextDisplay &td) {
	for (int i = 0; i < 17; i++) {
		std::vector<char> v;
		for (int j = 0; j < 13; j++) {
			std::cout << td.theDisplay[i][j];
		}
		std::cout << std::endl;
	}
	return out;
}
