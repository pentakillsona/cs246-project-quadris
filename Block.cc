#include "Block.h"
#include "Board.h"

bool Block::move(int x,int y) {
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		if (col+x<0||col+x > 10)return false;
		if (row + y<0 || row + y > 17)return false;
		if (board->dropped(row+y, col + x)) return false;
	}
	std::vector<Cell*> tmp;
	for (auto c : cells) {
		c->empty();
		tmp.emplace_back(c);
	}
	cells.clear();
	for (auto c : tmp) 
		cells.emplace_back(board->getCell(c->getState().r+y, c->getState().c + x));
	for (auto c : cells)
		c->fill(this->type);
	return true;
}

void Block::clockwise() {
	int minrow = 17, maxrow = 0, mincol = 10, maxcol = 0;
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		if (row < minrow)minrow = row;
		if (row > maxrow)maxrow = row;
		if (col < mincol)mincol = col;
		if (col > maxcol)maxcol = col;
	}
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		int newrow = maxrow - (maxcol - col);
		int newcol = mincol + maxrow - row;
		if (newcol<0 || newcol > 10)return;
		if (newrow<0 || newrow > 17)return;
		if (board->dropped(newrow, newcol)) return;
	}
	std::vector<Cell*> tmp;
	for (auto c : cells) {
		c->empty();
		tmp.emplace_back(c);
	}
	cells.clear();
	for (auto c : tmp) {
		int row = c->getState().r;
		int col = c->getState().c;
		int newrow = maxrow - (maxcol - col);
		int newcol = mincol + maxrow - row;
		cells.emplace_back(board->getCell(newrow, newcol));
	}
	for (auto c : cells)
		c->fill(this->type);
}

void Block::counterclockwise() {
	int minrow = 17, maxrow = 0, mincol = 10, maxcol = 0;
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		if (row < minrow)minrow = row;
		if (row > maxrow)maxrow = row;
		if (col < mincol)mincol = col;
		if (col > maxcol)maxcol = col;
	}
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		int newrow = maxrow - (col - mincol);
		int newcol = mincol + row - minrow;
		if (newcol<0 || newcol > 10)return;
		if (newrow<0 || newrow > 17)return;
		if (board->dropped(newrow, newcol)) return;
	}
	std::vector<Cell*> tmp;
	for (auto c : cells) {
		c->empty();
		tmp.emplace_back(c);
	}
	cells.clear();
	for (auto c : tmp) {
		int row = c->getState().r;
		int col = c->getState().c;
		int newrow = maxrow - (col - mincol);
		int newcol = mincol + row - minrow;
		cells.emplace_back(board->getCell(newrow, newcol));
	}
	for (auto c : cells)
		c->fill(this->type);
}

void Block::left() { move(-1, 0); }

void Block::right() { move(1, 0); }

bool Block::down() {return move(0, 1); }

void Block::drop(int level, int id) {
	while(down()){}
	for (auto c : cells) c->drop(level, id);
}

void Block::empty() {
	for (auto c : cells) c->empty();
}

void Block::fill(char type) {
	for (auto c : cells) c->fill(type);
}

void Block::save() {
	cellsSave.clear();
	for (auto c : cells) cellsSave.emplace_back(c);
	posSave = pos;
	type = cellsSave[0]->getState().type;
}

void Block::hintSave() {
	hintCells.clear();
	for (auto c : cells) hintCells.emplace_back(c);
}

void Block::hintClear() {
	for (auto c : hintCells) c->fill(' ');
	hintCells.clear();
}

void Block::undo() {
	empty();
	cells.clear();
	for (auto c : cellsSave) cells.emplace_back(c);
	fill(type);
	pos = posSave;
}

int Block::flyCount() {
	int count = 0;
	for (auto c : cells) {
		int row = c->getState().r;
		int col = c->getState().c;
		if (row < 17) {
			if (!board->getCell(row + 1, col)->filled())
				count++;
		}
	}
	return count;
}

bool Block::isempty() {
	if (cells.size() == 0)return true;
	return false;
}
