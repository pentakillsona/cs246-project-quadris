#include "IBlock.h"
#include "Board.h"

using namespace std;

IBlock::IBlock(Board *b, int lvl) {
    type = 'I';
    board = b;
    level = lvl;

    for (int c = 0; c < 4; ++c) {
        if (board->dropped(3, c)) {
            cells.clear();
			break;
        } else {
            cells.emplace_back(board->getCell(3, c));
        }
    }

    for (auto c: cells) c->fill(this->type);
}


IBlock::~IBlock() {}
