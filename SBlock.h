#ifndef SBLOCK_H
#define SBLOCK_H

#include "Block.h"

class Board;

class SBlock: public Block {
public:
	SBlock(Board*, int);
	~SBlock();
};


#endif