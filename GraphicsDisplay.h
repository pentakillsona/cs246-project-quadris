#ifndef GRAPHICSDISPLAY_H
#define GRAPHICSDISPLAY_H
#include <iostream>
#include <vector>
#include <string>
#include "subject.h"
#include "observer.h"
#include "state.h"
#include "window.h"
class Cell;

class GraphicsDisplay : public Observer<State> {
	Xwindow window;
  int level;
  int score;
  int hiscore;
  std::string next;
public:
	GraphicsDisplay();
  void init();
  void update(int level, int score, int hiscore, std::string next);
  void drawGameInfo();
	void notify(Subject<State> &whoNotified) override;
  void sync();
  void drawTetramino(std::string next);
};
#endif
