#include "OBlock.h"
#include "Board.h"

OBlock::OBlock(Board *b, int lvl) {
    type = 'O';
    board = b;
    level = lvl;

    for (int r = 4; r > 2; --r) {
        for (int c = 0; c < 2; ++c) {
            if (b->filled(r, c)) {
                cells.clear();
                return;
            } else {
                cells.emplace_back(board->getCell(r, c));
            }
        }
    }

    for (auto c: cells) c->fill(this->type);
}


OBlock::~OBlock() {}