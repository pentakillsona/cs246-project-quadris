#ifndef _BOARD_H_
#define _BOARD_H_
#include "Cell.h"
#include "Block.h"
#include "IBlock.h"
#include "JBlock.h"
#include "LBlock.h"
#include "OBlock.h"
#include "SBlock.h"
#include "TBlock.h"
#include "ZBlock.h"
#include "XBlock.h"
#include "TextDisplay.h"
#include "GraphicsDisplay.h"
#include <vector>
#include <string>
#include <fstream>

class Board
{
	const int numRows = 15;
	const int numCols = 11;
	std::vector<std::vector<Cell> > theBoard;
	Block* block;
	std::string next;
	TextDisplay* td;
	GraphicsDisplay* gd;
	int level;
	bool israndom;
	std::string blockList;
	int score=0;
  int hiscore=0;
	int badturnCount;
  std::vector<int> refs;
  int blockNum;
  std::string blockFile;
  std::ifstream commandFile;
  bool runFile;
	bool text = false;
	bool lvChange;

	int numTimes;
	std::string input;
	std::vector<std::string> commands;

	int readCount;
	std::ifstream myfile;

	// Produces the next block for the game, adding upcoming block to display,
	// follows rules from levels
	void newBlock(std::string);
	std::string blockSelector();
	std::string readFile();
	void runCommand(std::string);
	void Command();
	void interpretCommand(std::string);

	bool clear();
	void clearLine(int n);

	// level commands
	void levelup();
	void leveldown();

	// turns off random for this instance of the game, the sequence of blocks is
	// determined by an input file
	void norandom(std::string);

	// turns on random block generation
	void random();

	void sequence(std::string);
	void replaceBlock(std::string);

	// provides optimal placement for block
	void hint();
	int hintScore();

	void updateScore(int);
public:
	Board();
	~Board();


	// Initializes the game, attaching observers to subjects and creating
	// text/graphics displays. Erases the previous game if started, preserving
	// high scores
	void init(int startLevel, int seed, bool textFlag, std::string file);

	bool filled(int row, int col);
	bool dropped(int row, int col);
	Cell* getCell(int row, int col);

  friend std::ostream &operator<<(std::ostream &out, const Board &b);

};


#endif
