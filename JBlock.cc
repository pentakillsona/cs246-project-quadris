#include "JBlock.h"
#include "Board.h"

using namespace std;

JBlock::JBlock(Board *b, int lvl) {
    type = 'J';
    board = b;
    level = lvl;

    for (int c = 0; c < 3; ++c) {
        if (b->dropped(4, c)) {
            cells.clear();
            return;
        } else {
            cells.emplace_back(board->getCell(4, c));
        }
    }
    
    if (b->dropped(3, 0)) {
        cells.clear();
        return;
    } else {
        cells.emplace_back(board->getCell(3, 0));
    }

    for (auto c: cells) c->fill(this->type);
}


JBlock::~JBlock() {}