#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <vector>
#include "Cell.h"

class Board;

class Block {
	std::vector<Cell*> cellsSave;
	std::vector<Cell*> hintCells;
	int posSave;
public:
	virtual ~Block() = default;

	void left();
	void right();
	bool down();
	void drop(int level, int id);
	bool move(int x, int y);

	void clockwise();
	void counterclockwise();

	void empty();
	void fill(char);

	void save();
	void undo();
	void hintSave();
	void hintClear();
	int flyCount();

	bool isempty();

protected:
	char type;
	int pos = 1;
	int level;
	std::vector<Cell*> cells;
	Board *board;
};

#endif
