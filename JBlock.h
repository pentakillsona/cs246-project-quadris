#ifndef JBLOCK_H
#define JBLOCK_H

#include "Block.h"

class Board;

class JBlock: public Block {
public:
	JBlock(Board*, int);
	~JBlock();
};


#endif