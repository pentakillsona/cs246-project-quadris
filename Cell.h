#ifndef _CELL_H_
#define _CELL_H_
#include "subject.h"
#include "observer.h"
#include "state.h"
#include <string>

class Cell : public Subject<State> {
	const int r, c;
public:
	Cell(int,int);
	~Cell();

	// accessor methods
	bool filled();
	bool dropped();
	void empty();
	void fill(char);
	void drop(int level, int id);
	void clone(Cell &other);
  int getLevel();
  int getID();

};


#endif
