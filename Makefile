CXXFLAGS = -std=c++14 -Werror=vla -Wall -MMD -g
EXEC = quadris 
OBJECTS = main.o Block.o Cell.o Board.o GraphicsDisplay.o IBlock.o JBlock.o LBlock.o OBlock.o SBlock.o TBlock.o TextDisplay.o ZBlock.o window.o XBlock.o
HEADERS = state.h observer.h subject.h
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC} -lX11

-include ${DEPENDS}

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
.PHONY: clean
