#ifndef STATE_H
#define STATE_H

/* State types are:
?
*/
enum class StateType { /*?*/ };

struct State {
	int r, c;
	char type;
	bool dropped;
	int level;
  int blockID;
};

#endif
