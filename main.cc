#include <iostream>
#include <fstream>
#include <string>
#include "Board.h"
#include <getopt.h>

int startLevel = 0;
int seed = 0;
bool textOnlyFlag = false;
std::string file = "sequence.txt";

void help(char *me) {
  std::cout << "Usage " << me << "\t[-s --startlevel <level>]" << std::endl;
  std::cout << "\t\t[-r --seed <seed>]" << std::endl;
  std::cout << "\t\t[-t --text]" << std::endl;
  std::cout << "\t\t[-f --scriptfile<filename>]" << std::endl;
  exit(1);
}  


void ProcessArgs(int argc, char** argv) {
  const char* const short_opts = "s:r:t:f:h";
  const option long_opts[] = {
    {"startlevel", required_argument, nullptr, 's'},
    {"seed", required_argument, nullptr, 'r'},
    {"text", no_argument, nullptr, 't'},
    {"scriptfile", required_argument, nullptr, 'f'},
    {nullptr, no_argument, nullptr, 0}
  };
  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
    if (-1 == opt) break;
    switch (opt) {
      case 's':
          startLevel = std::stoi(optarg);
          break;
      case 'r':
          seed = std::stoi(optarg);
          break;
      case 't':
          textOnlyFlag = true;
          break;
      case 'f':
          file = std::string(optarg);
          break;
      case 'h':
      case '?': // Unrecognized option
      default:
          help(argv[0]);
          break;
      }
  }
}

int main(int argc, char *argv[]) {
	ProcessArgs(argc, argv);

	Board* b=new Board();
	try{b->init(startLevel, seed, textOnlyFlag, file);
	}
	catch (const char* msg) {
		std::cerr << msg << std::endl;
	}delete b;

	std::string word;
	std::cin >> word;
	return 0;
}
